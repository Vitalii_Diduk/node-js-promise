import * as utils from 'test-utils';
import * as util from 'util';

const params = {password: 'df345w43jkj3443d'};

const isPass = util.promisify(utils.runMePlease);

async function main () {
    try {
    const result = await isPass(params);
    console.log(result);
} catch (error) {
    console.log(error);
}
}

main();
